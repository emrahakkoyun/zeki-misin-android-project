package com.gojolo.zekimisin;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class X3 extends Activity {
	int time=20000;
	int d=0;
	int screenWidth,screenHeight;
	ImageView timeimg;
	int mt=0;
	static int count;
	Button btn1,btn2,btn3,btn4;
	boolean move;
	float bx1,by1,bx2,by2,bx3,by3;
	TextView soru;
	int t,f,s;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x3);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		count=0;
		t=0;f=0;s=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
		soru =(TextView)findViewById(R.id.soru);
		RelativeLayout.LayoutParams params = new 
        RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		params.setMargins((int)(screenWidth/28), (int)(screenHeight/3.34),0,0);
		soru.setTextSize((float)(screenWidth/35));
		soru.setLayoutParams(params);
		soru.setText("Tilki sincab� ,Sincap f�nd��� yer kar�� tarafa sadece birini g�t�rebilirsiniz! Nas�l?");
		btn1=new Button(this);
		btn2=new Button(this);
		btn3=new Button(this);
		btn4=new Button(this);
		RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(screenWidth/28,screenHeight/25);
		RelativeLayout.LayoutParams p3 = new RelativeLayout.LayoutParams(screenWidth/16,screenHeight/14);
		RelativeLayout.LayoutParams p4 = new RelativeLayout.LayoutParams(screenWidth/8,screenHeight/5);
		RelativeLayout.LayoutParams p5 = new RelativeLayout.LayoutParams(screenWidth/8,screenHeight/2);
		btn1.setLayoutParams(p2);
		btn2.setLayoutParams(p3);
		btn3.setLayoutParams(p4);
		btn4.setLayoutParams(p5);
		rlt.addView(btn1);
		rlt.addView(btn2);
		rlt.addView(btn3);
		rlt.addView(btn4);
	    btn1.setBackgroundResource(R.drawable.findik);
	    btn2.setBackgroundResource(R.drawable.sincap);
	    btn3.setBackgroundResource(R.drawable.tilki);
	    btn4.setBackgroundResource(R.drawable.sinir);
	    btn1.setX((int)(screenWidth/8.28));
	    btn1.setY((int)(screenHeight/2.26));
	    btn2.setX((int)(screenWidth/10.25));
	    btn2.setY((int)(screenHeight/1.75));
	    btn3.setX((int)(screenWidth/15.26));
	    btn3.setY((int)(screenHeight/1.40));
	    btn4.setX((int)(screenWidth/2.15));
	    btn4.setY((int)(screenHeight/2.26));
	    btn1.bringToFront();
	    btn2.bringToFront();
	    btn3.bringToFront();
	    btn1.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if(screenWidth/event.getRawX()<1.76&&screenWidth/event.getRawX()>1.19)
	        	{}
	        	else{
	        		karsitaraf();
	        	}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move)
			        	{
			        		bx1=event.getRawX()-btn1.getWidth()/2;
			        		by1=event.getRawY()-btn1.getHeight()/2;
			        		if(screenWidth/bx1<32.89&&screenWidth/bx1>1.11&&screenHeight/by1<2.38&&screenHeight/by1>1.15)
				        	{
			        			if(t==1&&s==1)
			        			{
					        		
			        			}
			        			else{
			        				btn1.setX(bx1);
					        		btn1.setY(by1);
			        			}
				        		if(screenWidth/bx1<1.74&&screenWidth/bx1>1.09)
					        	{
					        		f=1;
					        		
					        	}
					        	else{
					        		f=0;
					        	}
				        	}
				        }
			        	break;
			        }
				}
				return true;
			}
		});
		 btn2.setOnTouchListener(new View.OnTouchListener() {
				
			@Override
			public boolean onTouch(View v, MotionEvent event) {
	        	if(screenWidth/event.getRawX()<1.76&&screenWidth/event.getRawX()>1.19)
	        	{}
	        	else{
	        		karsitaraf();
	        	}
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move)
			        	{
			        		bx2=event.getRawX()-btn2.getWidth()/2;
			        		by2=event.getRawY()-btn2.getHeight()/2;
			        		if(screenWidth/bx2<28.53&&screenWidth/bx2>1.11&&screenHeight/by2<2.38&&screenHeight/by2>1.15)
				        	{
			        			
				        		btn2.setX(bx2);
				        		btn2.setY(by2);
			        			if(s==1&&t==1&&f==1)
			        			{
			        				gecis();
			        			}
				        		if(screenWidth/bx2<1.74&&screenWidth/bx2>1.11)
					        	{
					        		s=1;
					        		
					        	}
					        	else{
					        		s=0;
					        	}
				        	}
				        }
			        	break;
			        }
				}
				return true;
			}
		});
		btn3.setOnTouchListener(new View.OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
		        	if(screenWidth/event.getRawX()<1.76&&screenWidth/event.getRawX()>1.19)
		        	{}
		        	else{
		        		karsitaraf();
		        	}
					 switch (event.getAction() & MotionEvent.ACTION_MASK) {
				        case MotionEvent.ACTION_DOWN:
				        {

				        	move=true;
							break;
						}
				        case MotionEvent.ACTION_UP:
				        {
				        	
				        	move=false;
				        	break;
				        }
				        case MotionEvent.ACTION_MOVE:
				        {
				        	if(move)
				        	{
				        		bx3=event.getRawX()-btn3.getWidth()/2;
				        		by3=event.getRawY()-btn3.getHeight()/2;
				        	 if(screenWidth/bx3<30.52&&screenWidth/bx3>1.11&&screenHeight/by3<2.38&&screenHeight/by3>1.33)
					        	{
				        		 if(s==1&&f==1)
				        			{
						        		
				        			}
				        			else{
					        		btn3.setX(bx3);
					        		btn3.setY(by3);
				        			}
					        		if(screenWidth/bx3<1.76&&screenWidth/bx3>1.19)
						        	{
						        		t=1;
						        		
						        	}
						        	else{
						        		t=0;
						        	}
					        	}
					        	
					        }
				        	break;
				        }
					}
					return true;
				}
			});
		final  Intent glevel = new Intent(X3.this,Kayib.class);
	      CountDownTimer cnt=   new CountDownTimer(time, 1000) {

				  public void onTick(long millisUntilFinished) {
						timeimg.setX((float)(screenWidth/1.34)+mt);
						timeimg.setY((float)(screenHeight/8.51));
						mt-=35;
							if(count==0)
						{	if(millisUntilFinished/1000==1)
						{		finish();glevel.putExtra("level",3); startActivity(glevel);
						}
						}
				   //here you can have your logic to set text to edittext
				  }

				  public void onFinish() {
				       }
				 }
				 .start();
					
	}
	public void karsitaraf()
	{

	}
	public void  gecis()
	{
		count=1;
		finish();
		Intent i = new Intent(X3.this,Tebrik.class);
		i.putExtra("level",3);
		startActivity(i);
	}
}