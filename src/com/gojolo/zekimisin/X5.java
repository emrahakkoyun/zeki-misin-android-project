package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class X5 extends Activity {
	int time=20000;
	int screenWidth,screenHeight;
	ImageView timeimg;
	int mt=0;
	static int count;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x5);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		count=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
		Bundle extras = getIntent().getExtras();
		final  Intent glevel = new Intent(X5.this,Kayib.class);
      CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
					timeimg.setX((float)(screenWidth/1.34)+mt);
					timeimg.setY((float)(screenHeight/8.51));
					mt-=35;
					if(count==0)
					{	if(millisUntilFinished/1000==1)
					{		finish();glevel.putExtra("level",5); startActivity(glevel);
					}
					}
			   //here you can have your logic to set text to edittext
			  }

			  public void onFinish() {
			       }
			 }
			 .start();

			
	 }
	 public boolean onTouchEvent(MotionEvent event) {
		 if(screenWidth/event.getX()<1.76&&screenWidth/event.getX()>1.18&&screenHeight/event.getY()<1.34&&screenHeight/event.getY()>1.13)
		 {
			gecis();
		 }
		return true;

		}
	public void  gecis()
	{
		count=1;
		finish();
		Intent i = new Intent(X5.this,Tebrik.class);
		i.putExtra("level",5);
		startActivity(i);
	}

	 
	}
