package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.Toast;

public class MainActivity extends Activity {
	int screenWidth,screenHeight;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
	}
	 public boolean onTouchEvent(MotionEvent event) {
		 if(screenWidth/event.getX()<2.60&&screenWidth/event.getX()>1.67&&screenHeight/event.getY()<3.01&&screenHeight/event.getY()>1.34)
		 {
	         Intent level_menu = new Intent(MainActivity.this,X1.class);
	         startActivity(level_menu);
	         finish();
		 }
		return true;

		}
	
	}
