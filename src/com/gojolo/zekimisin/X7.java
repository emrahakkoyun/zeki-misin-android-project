package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class X7 extends Activity {
	int time=20000;
	static int count;
	ImageView timeimg;
	int screenWidth,screenHeight;
	int mt=0;
int d=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x7);
		count=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		final TextView timetext =(TextView)findViewById(R.id.textView1);
		final TextView soru =(TextView)findViewById(R.id.soru);
		soru.setText("Patlat");
		Bundle extras = getIntent().getExtras();
		final  Intent glevel = new Intent(X7.this,Kayib.class);
		glevel.putExtra("level",7);
		  final ImageView btn1 =(ImageView)findViewById(R.id.btn1);
		final  ImageView btn2 =(ImageView)findViewById(R.id.btn2);
		final  ImageView btn3 =(ImageView)findViewById(R.id.btn3);
		final  ImageView btn4 =(ImageView)findViewById(R.id.btn4);
		final  ImageView btn5 =(ImageView)findViewById(R.id.btn5);
		  btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				d=1;
				finish();
				startActivity(glevel);
				  
			}
		});
		  btn2.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					d=1;
					finish();
					startActivity(glevel);
					  
				}
			});
		  btn3.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					d=1;
					finish();
					Intent i = new Intent(X7.this,X8.class);
					glevel.putExtra("x7",true); 
					startActivity(i);
				}
			});
		  btn4.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					d=1;
					finish();
					startActivity(glevel);
				}
			});
		  btn5.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					d=1;
					finish();
					startActivity(glevel);
					  
				}
			});
		  CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
					timeimg.setX((float)(screenWidth/1.34)+mt);
					timeimg.setY((float)(screenHeight/8.51));
					mt-=35;
					if(count==0)
					{	if(millisUntilFinished/1000==1)
					{		finish();glevel.putExtra("level",7); startActivity(glevel);
					}
					}
			   //here you can have your logic to set text to edittext
			  }

			  public void onFinish() {
			       }
			 }
			 .start();
	}
public void  gecis()
{
	count=1;
	finish();
	Intent i = new Intent(X7.this,Tebrik.class);
	i.putExtra("level",7);
	startActivity(i);
}
}
