package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

public class Tebrik extends Activity {
int level;
int screenWidth,screenHeight;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tebrik);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		Bundle extras = getIntent().getExtras();
		level=extras.getInt("level");
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
	}
	 public boolean onTouchEvent(MotionEvent event) {
		 if(screenWidth/event.getX()<3.95&&screenWidth/event.getX()>2.49&&screenHeight/event.getY()<1.66&&screenHeight/event.getY()>1.14)
		 {
			 Intent nint = new Intent(Tebrik.this,Level.class);
			 if(level==1)
			 {nint.putExtra("x2",true);
			 }
			 if(level==2)
			 {nint.putExtra("x3",true);
			 }
			 if(level==3)
			 { nint.putExtra("x4",true);
			 }
			 if(level==4)
			 { nint.putExtra("x5",true);
			 }
			 if(level==5)
			 {nint.putExtra("x6",true);
			 }
			 if(level==6)
			 { nint.putExtra("x7",true);
			 }
			 if(level==7)
			 { nint.putExtra("x8",true);
			 }
			 if(level==8)
			 { nint.putExtra("x9",true);
			 }
			 if(level==9)
			 { nint.putExtra("x10",true);
			 }
			 if(level==10)
			 { nint.putExtra("x11",true);
			 }
			 if(level==11)
			 { nint.putExtra("x12",true);
			 }
			 if(level==12)
			 {nint.putExtra("x13",true);
			 }
			 if(level==13)
			 { nint.putExtra("x14",true);
			 }
			 if(level==14)
			 { nint.putExtra("x15",true);
			 }
			 if(level==15)
			 { nint.putExtra("x16",true);
			 }
			 if(level==16)
			 { nint.putExtra("x17",true);
			 }
			 if(level==17)
			 { nint.putExtra("x18",true);
			 }
			 if(level==18)
			 { nint.putExtra("x19",true);
			 }
			 if(level==19)
			 { nint.putExtra("x20",true);
			 }
			 if(level==20)
			 { nint.putExtra("x21",true);
			 }
			 if(level==21)
			 { nint.putExtra("x22",true);
			 }
			 if(level==22)
			 { nint.putExtra("x23",true);
			 }
			 if(level==23)
			 {  nint.putExtra("x24",true);
			 }
			 startActivity(nint);
			 finish();
		 }
		 if(screenWidth/event.getX()<1.75&&screenWidth/event.getX()>1.24&&screenHeight/event.getY()<1.66&&screenHeight/event.getY()>1.14)
		 {
			 if(level==1)
			 { Intent nint = new Intent(Tebrik.this,X2.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==2)
			 { Intent nint = new Intent(Tebrik.this,X3.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==3)
			 { Intent nint = new Intent(Tebrik.this,X4.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==4)
			 { Intent nint = new Intent(Tebrik.this,X5.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==5)
			 { Intent nint = new Intent(Tebrik.this,X6.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==6)
			 { Intent nint = new Intent(Tebrik.this,X7.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==7)
			 { Intent nint = new Intent(Tebrik.this,X8.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==8)
			 { Intent nint = new Intent(Tebrik.this,X9.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==9)
			 { Intent nint = new Intent(Tebrik.this,X10.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==10)
			 { Intent nint = new Intent(Tebrik.this,X11.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==11)
			 { Intent nint = new Intent(Tebrik.this,X12.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==12)
			 { Intent nint = new Intent(Tebrik.this,X13.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==13)
			 { Intent nint = new Intent(Tebrik.this,X14.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==14)
			 { Intent nint = new Intent(Tebrik.this,X15.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==15)
			 { Intent nint = new Intent(Tebrik.this,X16.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==16)
			 { Intent nint = new Intent(Tebrik.this,X17.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==17)
			 { Intent nint = new Intent(Tebrik.this,X18.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==18)
			 { Intent nint = new Intent(Tebrik.this,X19.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==19)
			 { Intent nint = new Intent(Tebrik.this,X20.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==20)
			 { Intent nint = new Intent(Tebrik.this,X21.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==21)
			 { Intent nint = new Intent(Tebrik.this,X22.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==22)
			 { Intent nint = new Intent(Tebrik.this,X23.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==23)
			 { Intent nint = new Intent(Tebrik.this,X24.class);
			 startActivity(nint);
			 finish();
			 }
			 if(level==24)
			 { Intent nint = new Intent(Tebrik.this,X25.class);
			 startActivity(nint);
			 finish();
			 }
			 
		 }
		return true;

		}
}
