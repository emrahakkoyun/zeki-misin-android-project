package com.gojolo.zekimisin;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class X4 extends Activity {
	int time=20000;
	int screenWidth,screenHeight;
	ImageView timeimg;
	int mt=0;
	static int count;
	Button btn1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x4);
		count=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
		final TextView soru =(TextView)findViewById(R.id.soru);
		RelativeLayout.LayoutParams params = new 
        RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		params.setMargins((int)(screenWidth/3.00), (int)(screenHeight/3.34),0,0);
		soru.setTextSize((float)(screenWidth/30));
		soru.setLayoutParams(params);
		soru.setText("Saati 02:45'e ayarla!");
		Bundle extras = getIntent().getExtras();
		final  Intent glevel = new Intent(X4.this,Kayib.class);
      CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
					timeimg.setX((float)(screenWidth/1.34)+mt);
					timeimg.setY((float)(screenHeight/8.51));
					mt-=35;
					if(count==0)
					{	if(millisUntilFinished/1000==1)
					{		finish();glevel.putExtra("level",4); startActivity(glevel);
					}
					}
			   //here you can have your logic to set text to edittext
			  }

			  public void onFinish() {
			       }
			 }
			 .start();
				btn1=new Button(this);
				RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(screenWidth/7,screenHeight/4);
				btn1.setLayoutParams(p2);
				rlt.addView(btn1);
			    btn1.setBackgroundResource(R.drawable.clock);
			    btn1.setX((int)(screenWidth/2.38));
			    btn1.setY((int)(screenHeight/2.00));
			    btn1.setTextSize((float)(screenWidth/30));
			    btn1.setOnTouchListener(new View.OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(screenWidth/event.getX()<9.31&&screenWidth/event.getX()>7.46&&screenHeight/event.getY()<15.11&&screenHeight/event.getY()>5.97)
						{
						    btn1.setBackgroundResource(R.drawable.clock1);
						   gecis();
						}
						if(screenWidth/event.getX()<23.90&&screenWidth/event.getX()>11.27&&screenHeight/event.getY()<5.77&&screenHeight/event.getY()>4.32)
						{
						    btn1.setBackgroundResource(R.drawable.clock2);
						}
						if(screenWidth/event.getX()<149.39&&screenWidth/event.getX()>43.30&&screenHeight/event.getY()<15.11&&screenHeight/event.getY()>5.97)
						{
						    btn1.setBackgroundResource(R.drawable.clock3);
						    //cevap
						}
						if(screenWidth/event.getX()<23.90&&screenWidth/event.getX()>11.27&&screenHeight/event.getY()<103&&screenHeight/event.getY()>19.48)
						{
						    btn1.setBackgroundResource(R.drawable.clock);
						    //cevap
						}
						return false;
					}
				});
	}
	public void  gecis()
	{
		count=1;
		finish();
		Intent i = new Intent(X4.this,Tebrik.class);
		i.putExtra("level",4);
		startActivity(i);
	}
}
