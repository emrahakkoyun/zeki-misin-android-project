package com.gojolo.zekimisin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class X23 extends Activity {
static int time=50000;
int d=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x23);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		final TextView timetext =(TextView)findViewById(R.id.textView1);
		final TextView soru =(TextView)findViewById(R.id.soru);
		soru.setText("Kaçıncı bölümdesiniz?");
		Bundle extras = getIntent().getExtras();
		final  Intent glevel = new Intent(X23.this,Level.class);
		glevel.putExtra("x1",true); 
		glevel.putExtra("x2",true); 
		glevel.putExtra("x3",true); 
		glevel.putExtra("x4",true);
		glevel.putExtra("x5",true); 
		glevel.putExtra("x6",true); 
		glevel.putExtra("x7",true); 
		glevel.putExtra("x8",true); 
		glevel.putExtra("x9",true); 
		glevel.putExtra("x10",true); 
		glevel.putExtra("x11",true); 
		glevel.putExtra("x12",true); 
		glevel.putExtra("x13",true); 
		glevel.putExtra("x14",true);
		glevel.putExtra("x15",true); 
		glevel.putExtra("x16",true); 
		glevel.putExtra("x17",true); 
		glevel.putExtra("x18",true); 
		glevel.putExtra("x19",true); 
		glevel.putExtra("x20",true); 
		glevel.putExtra("x21",true); 
		glevel.putExtra("x22",true); 
	final	CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
			      timetext.setText("" + millisUntilFinished / 1000);
			   //here you can have your logic to set text to edittext
			  }

			  public void onFinish() {
				  finish();
				  if(d==0)
					startActivity(glevel);
			  }
			 }
			 .start();
		  final Button btn1 =(Button )findViewById(R.id.btn1);
		final  Button btn2 =(Button )findViewById(R.id.btn2);
		final  Button btn3 =(Button )findViewById(R.id.btn3);
		final Button  btn4 =(Button )findViewById(R.id.btn4);
		final  Button  btn5 =(Button )findViewById(R.id.btn5);
		btn1.setText("19");
		btn2.setText("20");
		btn3.setText("21");
		btn4.setText("22");
		btn5.setText("23");
		  btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				d=1;
				startActivity(glevel);
				  
			}
		});
		  btn2.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					finish();
					d=1;
					startActivity(glevel);
					  
				}
			});
		  btn3.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					finish();
					d=1;
					startActivity(glevel);
				}
			});
		  btn4.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					finish();
					d=1;
					startActivity(glevel);
				}
			});
		  btn5.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					finish();
					d=1;
					Intent i = new Intent(X23.this,X24.class);
					glevel.putExtra("x23",true); 
					startActivity(i);
					  
				}
			});
	}
}
