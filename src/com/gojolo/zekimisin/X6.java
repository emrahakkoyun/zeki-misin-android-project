package com.gojolo.zekimisin;

import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class X6 extends Activity {
	int time=20000;
	int screenWidth,screenHeight;
	ImageView timeimg;
	int mt=0;
	static int count;
	boolean move;
	Button btn1,btn2,btn3,btn4,btn5;
	float nx,ny;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x6);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		count=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
	    final TextView soru =(TextView)findViewById(R.id.soru);
		RelativeLayout.LayoutParams params = new 
        RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		params.setMargins((int)(screenWidth/3.00), (int)(screenHeight/3.34),0,0);
		soru.setTextSize((float)(screenWidth/30));
		soru.setLayoutParams(params);
		soru.setText("kibrit ile �nce hangisini yakars�n�z?");
		btn1=new Button(this);
		btn2=new Button(this);
		btn3=new Button(this);
		btn4=new Button(this);
		btn5=new Button(this);
		RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(screenWidth/8,screenHeight/5);
		btn1.setLayoutParams(p2);
		btn2.setLayoutParams(p2);
		btn3.setLayoutParams(p2);
		btn4.setLayoutParams(p2);
		btn5.setLayoutParams(p2);
		rlt.addView(btn1);
		rlt.addView(btn2);
		rlt.addView(btn3);
		rlt.addView(btn4);
		rlt.addView(btn5);
	    btn1.setBackgroundResource(R.drawable.s61);
	    btn2.setBackgroundResource(R.drawable.s62);
	    btn3.setBackgroundResource(R.drawable.s63);
	    btn4.setBackgroundResource(R.drawable.s64);
	    btn5.setBackgroundResource(R.drawable.s65);
	    btn1.setX((int)(screenWidth/6.63));
	    btn1.setY((int)(screenHeight/1.40));
	    btn2.setX((int)(screenWidth/3.54));
	    btn2.setY((int)(screenHeight/1.40));
	    btn3.setX((int)(screenWidth/2.38));
	    btn3.setY((int)(screenHeight/1.40));
	    btn4.setX((int)(screenWidth/1.79));
	    btn4.setY((int)(screenHeight/1.40));
	    btn5.setX((int)(screenWidth/1.42));
	    btn5.setY((int)(screenHeight/1.40));
	    btn1.setTextSize((float)(screenWidth/30));
	    btn2.setTextSize((float)(screenWidth/30));
	    btn3.setTextSize((float)(screenWidth/30));
	    btn4.setTextSize((float)(screenWidth/30));
	    btn5.setTextSize((float)(screenWidth/30));
		 btn5.setOnTouchListener(new View.OnTouchListener() {
				
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move)
			        	{
			        		nx=event.getRawX()-btn5.getWidth()/2;
			        		ny=event.getRawY()-btn5.getHeight()/2;
			        		if(screenWidth/nx<30.52&&screenWidth/nx>1.11&&screenHeight/ny<18.82&&screenHeight/ny>1.15)
				        	{
			        	 		btn5.setX(nx);
					        	btn5.setY(ny);
					        	if(screenWidth/nx<1.85&&screenWidth/nx>1.75&&screenHeight/ny<1.51&&screenHeight/ny>1.35)
					        	{
					        		gecis();
					        	}
					        	if(screenWidth/nx<2.61&&screenWidth/nx>2.26&&screenHeight/ny<1.51&&screenHeight/ny>1.35)
					        	{
					        		
					        	}
					        	if(screenWidth/nx<3.62&&screenWidth/nx>3.39&&screenHeight/ny<1.51&&screenHeight/ny>1.35)
					        	{
					        		
					        	}
					        	if(screenWidth/nx<9.85&&screenWidth/nx>5.84&&screenHeight/ny<1.51&&screenHeight/ny>1.35)
					        	{
					        		
					        	}
				        	}
				        }
			        	break;
			        }
				}
				return true;
			}
		});
		Bundle extras = getIntent().getExtras();
		final  Intent glevel = new Intent(X6.this,Kayib.class);
      CountDownTimer cnt=   new CountDownTimer(time, 1000) {

			  public void onTick(long millisUntilFinished) {
					timeimg.setX((float)(screenWidth/1.34)+mt);
					timeimg.setY((float)(screenHeight/8.51));
					mt-=35;
					if(count==0)
					{	if(millisUntilFinished/1000==1)
					{		finish();glevel.putExtra("level",6); startActivity(glevel);
					}
					}
			   //here you can have your logic to set text to edittext
			  }

			  public void onFinish() {
			       }
			 }
			 .start();
	}
	public void  gecis()
	{
		count=1;
		finish();
		Intent i = new Intent(X6.this,Tebrik.class);
		i.putExtra("level",6);
		startActivity(i);
	}

}
