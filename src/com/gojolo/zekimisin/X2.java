package com.gojolo.zekimisin;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class X2 extends Activity{
	int time=20000;
	int d=0;
	int screenWidth,screenHeight;
	ImageView timeimg;
	int mt=0;
	static int count;
	Button btn1,btn2,btn3;
	boolean move;
	float nx,ny,rx,ry;
	TextView soru;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_x2);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);  
		count=0;
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		RelativeLayout rlt = (RelativeLayout)findViewById(R.id.rlt);
		timeimg=new ImageView(this);
		RelativeLayout.LayoutParams p1 = new RelativeLayout.LayoutParams(screenWidth/11,screenHeight/7);
		timeimg.setLayoutParams(p1);
		rlt.addView(timeimg);
	    timeimg.setBackgroundResource(R.drawable.monkey);
		soru =(TextView)findViewById(R.id.soru);
		RelativeLayout.LayoutParams params = new 
        RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT); 
		params.setMargins((int)(screenWidth/4.60), (int)(screenHeight/3.34),0,0);
		soru.setTextSize((float)(screenWidth/30));
		soru.setLayoutParams(params);
		soru.setText("Bu Elmay� Bu �i�enin i�ine nas�l katars�n�z?");
		btn1=new Button(this);
		btn2=new Button(this);
		btn3=new Button(this);
		RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(screenWidth/8,screenHeight/5);
		RelativeLayout.LayoutParams p3 = new RelativeLayout.LayoutParams(screenWidth/16,screenHeight/14);
		RelativeLayout.LayoutParams p4 = new RelativeLayout.LayoutParams(screenWidth/28,screenHeight/25);
		btn1.setLayoutParams(p2);
		btn2.setLayoutParams(p3);
		btn3.setLayoutParams(p4);
		rlt.addView(btn1);
		rlt.addView(btn2);
		rlt.addView(btn3);
	    btn1.setBackgroundResource(R.drawable.sise);
	    btn2.setBackgroundResource(R.drawable.apple);
	    btn3.setBackgroundResource(R.drawable.tohum);
	    btn1.setX((int)(screenWidth/2.38));
	    btn1.setY((int)(screenHeight/1.60));
	    btn2.setX((int)(screenWidth/1.79));
	    btn2.setY((int)(screenHeight/1.40));
	    btn3.setX((int)(screenWidth/1.79));
	    btn3.setY((int)(screenHeight/1.40));
		btn3.setEnabled(false);
		btn3.setVisibility(View.GONE);
		 btn2.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move)
			        	{
			        		nx=event.getRawX()-btn2.getWidth()/2;
			        		ny=event.getRawY()-btn2.getHeight()/2;
			        	 	if(screenWidth/nx<30.52&&screenWidth/nx>1.11&&screenHeight/ny<18.82&&screenHeight/ny>1.15)
				        	{
				        		if(screenWidth/nx<2.53&&screenWidth/nx>1.98&&screenHeight/ny<1.78&&screenHeight/ny>1.22)
				        		{
				        			
				        		}
				        		else{
					        	btn2.setX(nx);
					        	btn2.setY(ny);
					        	btn3.setX(nx);
					        	btn3.setY(ny);
				        		}
				        	}
				        }
			        	break;
			        }
				}
				return true;
			}
		});
		 btn3.setOnTouchListener(new View.OnTouchListener() {
				
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 switch (event.getAction() & MotionEvent.ACTION_MASK) {
			        case MotionEvent.ACTION_DOWN:
			        {

			        	move=true;
						break;
					}
			        case MotionEvent.ACTION_UP:
			        {
			        	move=false;
			        	break;
			        }
			        case MotionEvent.ACTION_MOVE:
			        {
			        	if(move)
			        	{
			        		nx=event.getRawX()-btn3.getWidth()/2;
			        		ny=event.getRawY()-btn3.getHeight()/2;
			        		if(screenWidth/nx<30.52&&screenWidth/nx>1.11&&screenHeight/ny<18.82&&screenHeight/ny>1.15)
				        	{
				        		if(screenWidth/nx<2.53&&screenWidth/nx>1.98&&screenHeight/ny<1.78&&screenHeight/ny>1.22)
				        		{
				        		gecis();	
				        		}
						        	btn3.setX(nx);
						        	btn3.setY(ny);
					        		
				        	}
				        }
			        	break;
			        }
				}
				return true;
			}
		});
		 rlt.setOnTouchListener(new View.OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					 switch (event.getAction() & MotionEvent.ACTION_MASK) {
				        case MotionEvent.ACTION_DOWN:
				        {

				        	move=true;
							break;
						}
				        case MotionEvent.ACTION_UP:
				        {
				        	move=false;
				        	break;
				        }
				        case MotionEvent.ACTION_MOVE:
				        {
				        	if(move)
				        	{
				        		rx=event.getRawX()-btn2.getWidth()/2;
				        		ry=event.getRawY()-btn2.getHeight()/2;
				        			if(screenWidth/rx<screenWidth/nx+10&&screenWidth/rx>screenWidth/nx-10&&screenHeight/ry<screenHeight/ny+10&&screenHeight/ry>screenHeight/ny-10)
					        		{
					        			btn2.setVisibility(View.GONE);
					        			btn3.setVisibility(View.VISIBLE);
					        			btn3.setEnabled(true);
					        			btn2.setEnabled(false);
					        		}
					        	
					        }
				        	break;
				        }
					}
					return true;
				}
			});
		final  Intent glevel = new Intent(X2.this,Kayib.class);
	      CountDownTimer cnt=   new CountDownTimer(time, 1000) {

				  public void onTick(long millisUntilFinished) {
						timeimg.setX((float)(screenWidth/1.34)+mt);
						timeimg.setY((float)(screenHeight/8.51));
						mt-=35;
						if(count==0)
						{	if(millisUntilFinished/1000==1)
						{		finish();glevel.putExtra("level",2); startActivity(glevel);
						}
						}
				   //here you can have your logic to set text to edittext
				  }

				  public void onFinish() {
				       }
				 }
				 .start();
					
	}
	public void  gecis()
	{
		count=1;
		finish();
		Intent i = new Intent(X2.this,Tebrik.class);
		i.putExtra("level",2);
		startActivity(i);
	}
}
